apache-directory-api (1.0.0-2) unstable; urgency=medium

  * Removed the dependency on libjaxp1.3-java
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 21 Jan 2021 16:53:13 +0100

apache-directory-api (1.0.0-1) unstable; urgency=medium

  * Team upload.
  * Watch official signed upstream release tarballs
  * New upstream release (1.0.0)
  * Use https in debian/watch
  * Remove 02-CVE-2015-3250.patch (fixed in M31)
  * Update for 1.0.0:
      * debian/libapache-directory-api-java.poms
      * debian/maven.ignoreRules
      * debian/maven.rules
  * Comment out ignore rules which can be ignored
  * Update debian/copyright
  * Install NOTICE with libapache-directory-api-java to comply with Apache-2.0
  * Bump debhelper compat from 9 to 11
  * Bump Standards-Version from 3.9.8 to 4.1.3 (no change required)
  * Remove unnecessary greater-than versioned dependency
  * Add mockito-core to maven.ignoreRules

 -- Christopher Hoskin <mans0954@debian.org>  Sat, 27 Jan 2018 09:07:25 +0000

apache-directory-api (1.0.0~M20-5) unstable; urgency=medium

  * Depend on libfindbugs-annotations-java instead of findbugs

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 19 Aug 2016 23:35:37 +0200

apache-directory-api (1.0.0~M20-4) unstable; urgency=medium

  * Added the missing build dependency on antlr (Closes: #828892)
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 3.9.8
  * Use a secure Vcs-Git URL

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 28 Jun 2016 22:31:34 +0200

apache-directory-api (1.0.0~M20-3) unstable; urgency=medium

  * Fixed CVE-2015-3250: Timing Attack vulnerability (Closes: #791957)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 09 Jul 2015 23:07:02 +0200

apache-directory-api (1.0.0~M20-2) unstable; urgency=medium

  * Ignore the parent pom for the api-all artifact

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 01 Jul 2015 17:14:53 +0200

apache-directory-api (1.0.0~M20-1) unstable; urgency=medium

  * Initial release (Closes: #789679)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 25 Jun 2015 00:13:11 +0200
